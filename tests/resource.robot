*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${SERVER}         localhost:8080
${BROWSER}        Chrome
${DELAY}          0
${START URL}      http://${SERVER}/Square2
${LOGIN URL}      http://${SERVER}/Square2/pages/login.jsf

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${START URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Go To Login Page
    Go To    ${START URL}
    Location Should Be ${LOGIN URL}

Input Username
    [Arguments]    ${username}
    Input Text    //input[@id='form:username']    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    //input[@id='form:password']    ${password}

Submit Credentials
    Click Button    //button[@id='form:btn_login']

Welcome Page Should Be Open
    Element Should Be Visible   //li[@id='form:topmenu_openversion']

Error Should Be Notified
    Element Should Be Visible   css:.ui-messages-error

Login With Credentials
    [Arguments]     ${username}     ${password}
    Open Browser To Login Page
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    Welcome Page Should Be Open

Go To Studyoverview Page
    Click Link    //a[contains(@id,'topmenu_tocohort')]
    Element Should Be Visible   //*[contains(@id,'datacollections')]

Go To Cohort Page
    Go To Studyoverview Page
    Click Button    //button[contains(@id, 'formbuttons') and contains(@id, 'btn_cancel')]

Make Cohort Entry
    Click Button    //button[contains(@id, 'formbuttons') and contains(@id, 'btn_add')]
    Element Should Be Visible   //input[contains(@id,'form:name')]
    Input Text  //input[contains(@id,'form:name')]  Studienname_hier
    Click Button  //button[contains(@id,'btn_add')]
    Page Should Contain     Studienname_hier

