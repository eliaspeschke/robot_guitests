** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Create New Cohort
    Login With Credentials    ${user}    ${pass}
    Go To Cohort Page
    Make Cohort Entry
    [Teardown]    Close Browser




