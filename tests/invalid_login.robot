*** Settings ***
Documentation     A test suite with a single test for invalid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Invalid Login
    Open Browser To Login Page
    Input Username    invalid
    Input Password    combination
    Submit Credentials
    Error Should Be Notified
    [Teardown]    Close Browser
