# Dependencies: 
```
    > python3 -m pip install robotframework
    > python3 -m pip install --upgrade robotframework-seleniumlibrary
```
# Configure:
Change server address in tests/resource.robot to the server where your desired Square2-instance is running

# Run:
```
    > robot --variable user:your-username-here --variable pass:your-password-here tests
```
